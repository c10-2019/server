﻿//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2012 Tasharen Entertainment
//----------------------------------------------

using System;
using UnityEngine;

/// <summary>
/// Sends a message to the remote object when something happens.
/// </summary>

[AddComponentMenu("NGUI/Interaction/Button Message")]
public class UIButtonMessage : MonoBehaviour
{
	public enum Trigger
	{
		OnClick,
		OnMouseOver,
		OnMouseOut,
		OnPress,
		OnRelease,
	}

    /// <summary>
    /// Лист игроков
    /// </summary>
    public UIGrid PlayerList;

	public GameObject target;
	public string functionName;
	public Trigger trigger = Trigger.OnClick;
	public bool includeChildren = false;
    public string argument;
    public bool SendArgument;

	void OnHover (bool isOver)
	{
		if (((isOver && trigger == Trigger.OnMouseOver) ||
			(!isOver && trigger == Trigger.OnMouseOut))) Send();
	}

	void OnPress (bool isPressed)
	{
		if (((isPressed && trigger == Trigger.OnPress) ||
			(!isPressed && trigger == Trigger.OnRelease))) Send();
	}

	void OnClick ()
	{
		if (trigger == Trigger.OnClick) Send();
	}

	void Send ()
	{
		if (!enabled || !gameObject.active || string.IsNullOrEmpty(functionName)) return;
		if (target == null) target = gameObject;

        if (functionName == "Locate") {
            try {
                PlayerList_Item[] list_pli = PlayerList.GetComponentsInChildren<PlayerList_Item>();

                for (int i = 0; i < list_pli.Length; i++) {
                    foreach (var user in MCSGlobalSimulation.Players.List.Values) {
                        if (list_pli[i].Player.externalIP == user.NetworkPlayer.externalIP) {
                            user.Account.FirstName = list_pli[i].PlayerName.text;
                        }
                    }
                }
            } catch (Exception e) { }
        }

		if (includeChildren)
		{
			Transform[] transforms = target.GetComponentsInChildren<Transform>();

			foreach (Transform t in transforms)
			{
				t.gameObject.SendMessage(functionName, gameObject, SendMessageOptions.DontRequireReceiver);
			}
		}
		else
		{
            if(!SendArgument)
			    target.SendMessage(functionName, gameObject, SendMessageOptions.DontRequireReceiver);
            else
                target.SendMessage(functionName, argument, SendMessageOptions.DontRequireReceiver);
		}
	}
}